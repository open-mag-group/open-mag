## Intro
The basic idea is to create 'small' Functions of the language currently supported by Open FaaS. The fastes and most efficient one wins.

### Getting started (all courses are free)
- [Docker and Image creation](https://kodekloud.com/)

- [OpenFaas Workshop with Python](https://github.com/openfaas/workshop)

- [Muenchen Mag](https://www.munichmag.de)